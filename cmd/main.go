package main

import (
	"crud1/database"
	"crud1/router"
)

func main() {
	database.Connect()
	router.NewRouter()
}
