package database

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

const (
	user     = "root"
	password = "51042970Txm@21"
	dbname   = "api_1"
)

//Conectando com banco de dados
func Connect() (*sql.DB, error) {
	strConnect := fmt.Sprintf("%s:%s@/%s", user, password, dbname)

	db, err := sql.Open("mysql", strConnect)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	if err = db.Ping(); err != nil {
		fmt.Println(err)
		return nil, err
	}

	fmt.Println("Conectado com sucesso")

	return db, nil
}
