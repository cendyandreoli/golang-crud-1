package router

import (
	"crud1/server"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func NewRouter() {
	router := mux.NewRouter()

	router.HandleFunc("/user", server.CreateUser).Methods(http.MethodPost)
	router.HandleFunc("/users", server.GetAllUsers).Methods(http.MethodGet)
	router.HandleFunc("/user/{id}", server.GetUserById).Methods(http.MethodGet)
	router.HandleFunc("/user/{id}", server.UpdateUser).Methods(http.MethodPut)
	router.HandleFunc("/user/{id}", server.SoftDeleteUser).Methods(http.MethodDelete)
	router.HandleFunc("/user/delete/{id}", server.DeleteUser).Methods(http.MethodDelete)

	fmt.Println("Server rodando na porta 5000")
	log.Fatal(http.ListenAndServe(":5000", router))
}
