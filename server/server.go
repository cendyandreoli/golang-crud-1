package server

import (
	"crud1/database"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type user struct {
	ID     int32  `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Active bool   `json:"active"`
}

// CreateUser Funcao para inserir usuário no banco de dados
func CreateUser(resp http.ResponseWriter, req *http.Request) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		respError(resp, fmt.Sprintf("Body is missing %s", err), http.StatusBadRequest)
		return
	}

	var user user
	if err = json.Unmarshal(body, &user); err != nil {
		respError(resp, fmt.Sprintf("Error to deserialize json %s", err), http.StatusInternalServerError)
	}

	fmt.Println(user)

	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot connect database %s", err), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	//PREPARE STATEMENT
	statement, err := db.Prepare(`insert into users (name, email) values (?, ?)`)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot create statement %s", err), http.StatusInternalServerError)
		return
	}

	defer statement.Close()

	insert, err := statement.Exec(user.Name, user.Email)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot execute statement %s", err), http.StatusInternalServerError)
		return
	}

	id, err := insert.LastInsertId()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot recovery id from database%s", err), http.StatusBadRequest)
		return
	}
	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusCreated)
	if err = json.NewEncoder(resp).Encode(id); err != nil {
		respError(resp, fmt.Sprintf("fmt.Sprintf( %s", err), http.StatusInternalServerError)
		return
	}

}

// GetAllUsers RETURN ALL USERS FROM DATABASE
func GetAllUsers(resp http.ResponseWriter, req *http.Request) {
	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot connect database %s", err), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	rows, err := db.Query(`SELECT * FROM USERS WHERE ACTIVE = TRUE`)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot find users in database %s", err), http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	var users []user
	for rows.Next() {
		var user user

		if err = rows.Scan(&user.ID, &user.Name, &user.Email, &user.Active); err != nil {
			respError(resp, fmt.Sprintf("Cannot read users in database %s", err), http.StatusInternalServerError)
			return
		}

		users = append(users, user)
	}

	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if err = json.NewEncoder(resp).Encode(users); err != nil {
		respError(resp, fmt.Sprintf("Cannot parser users to json %s", err), http.StatusInternalServerError)
		return
	}
}

// GetUserById RETURN A USERS BY ID FROM DATABASE
func GetUserById(resp http.ResponseWriter, req *http.Request) {
	ID, err := getUserRequestParam(resp, req)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot get param ID %s", err), http.StatusBadRequest)
		return
	}

	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot conncet database %s", err), http.StatusBadRequest)
		return
	}

	row, err := db.Query(`SELECT * FROM USERS WHERE ACTIVE = TRUE AND ID = ?`, ID)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot find user with id %d in database %s", ID, err), http.StatusInternalServerError)
		return
	}
	defer row.Close()

	var user user
	if row.Next() {
		if err = row.Scan(&user.ID, &user.Name, &user.Email, &user.Active); err != nil {
			respError(resp, fmt.Sprintf("Cannot read user in database %s", err), http.StatusInternalServerError)
			return
		}
	}

	if user.ID == 0 {
		respError(resp, fmt.Sprintf("User not found with id %d", user.ID), http.StatusBadRequest)
		return
	}

	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if err = json.NewEncoder(resp).Encode(user); err != nil {
		respError(resp, fmt.Sprintf("Cannot parser user to json %s", err), http.StatusInternalServerError)
		return
	}

}

// UpdateUser Funcao para atualizar usuário no banco de dados
func UpdateUser(resp http.ResponseWriter, req *http.Request) {
	ID, err := getUserRequestParam(resp, req)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot get param ID %s", err), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		respError(resp, fmt.Sprintf("Body is missing %s", err), http.StatusBadRequest)
		return
	}

	var user user
	if err = json.Unmarshal(body, &user); err != nil {
		respError(resp, fmt.Sprintf("Error to deserialize json %s", err), http.StatusInternalServerError)
	}

	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot connect database %s", err), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	//PREPARE STATEMENT
	statement, err := db.Prepare(`update users set name = ?, email = ? where id = ?`)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot create statement %s", err), http.StatusInternalServerError)
		return
	}

	defer statement.Close()

	_, err = statement.Exec(user.Name, user.Email, ID)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot execute statement %s", err), http.StatusInternalServerError)
		return
	}

	resp.Header().Set("Content-Type", "application/json")
	resp.WriteHeader(http.StatusOK)
	if err = json.NewEncoder(resp).Encode(ID); err != nil {
		respError(resp, fmt.Sprintf("fmt.Sprintf( %s", err), http.StatusInternalServerError)
		return
	}

}

// SoftDeleteUser Funcao para fazer um soft delete no usuário no banco de dados
func SoftDeleteUser(resp http.ResponseWriter, req *http.Request) {
	ID, err := getUserRequestParam(resp, req)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot get param ID %s", err), http.StatusBadRequest)
		return
	}

	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot connect database %s", err), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	//PREPARE STATEMENT
	statement, err := db.Prepare(`update users set active = ? where id = ?`)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot create statement %s", err), http.StatusInternalServerError)
		return
	}

	defer statement.Close()

	_, err = statement.Exec(false, ID)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot execute statement %s", err), http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusOK)
}

// DeleteUser Funcao para deletar o usuário no banco de dados
func DeleteUser(resp http.ResponseWriter, req *http.Request) {
	ID, err := getUserRequestParam(resp, req)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot get param ID %s", err), http.StatusBadRequest)
		return
	}

	db, err := database.Connect()
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot connect database %s", err), http.StatusInternalServerError)
		return
	}
	defer db.Close()

	//PREPARE STATEMENT
	statement, err := db.Prepare(`delete from users where id = ?`)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot create statement %s", err), http.StatusInternalServerError)
		return
	}

	defer statement.Close()

	_, err = statement.Exec(ID)
	if err != nil {
		respError(resp, fmt.Sprintf("Cannot execute statement %s", err), http.StatusInternalServerError)
		return
	}

	resp.WriteHeader(http.StatusOK)
}

func respError(resp http.ResponseWriter, message string, status int) {
	resp.WriteHeader(status)
	resp.Write([]byte(message))
}

func getUserRequestParam(resp http.ResponseWriter, req *http.Request) (uint64, error) {
	params := mux.Vars(req)
	ID, err := strconv.ParseUint(params["id"], 10, 16)
	return ID, err
}
